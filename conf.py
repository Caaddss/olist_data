import pandas as pd

def lendo_dataset(nome_csv):   
    import os
    import pandas as pd
    cwd = os.getcwd()
    try: 
        dataset = pd.read_csv(cwd+f'\work-at-olist-data\datasets\olist_{nome_csv}_dataset.csv',delimiter=',', encoding='utf-8')
    except:
        dataset = pd.read_csv(cwd+f'\work-at-olist-data\datasets\{nome_csv}.csv',
        delimiter=',', encoding='utf-8')
    return dataset

# adaptado de https://gist.github.com/ricardobeat/674646


